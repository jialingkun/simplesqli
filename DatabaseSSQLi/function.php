<?php
/*
Created by: Bent_Hartz
Updated: 8/6/2016
*/ 
include "connectionConfig.php";
$sqli = mysqli_connect($server,$user,$password,$database);
if (!$sqli){
	die("Database Connection Failed: ".mysqli_connect_error());
}
mysqli_set_charset($sqli,"utf8");

function query($content){
	global $sqli;
	$result = mysqli_query($sqli, $content);
	return $result;
}

function query_assoc($content){
	$data = query($content);
	$result=mysqli_fetch_all($data,MYSQLI_ASSOC);
	return $result;
}

function numrows($content){
	$result = mysqli_num_rows($content);
	return $result;
}

function query_numrows($content){
	$data = query($content);
	$result = mysqli_num_rows($data);
	return $result;
}

?>