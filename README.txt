Go to DatabaseSSQLi/connectionConfig.php to configure the database connection, 
then use it like the one in example.php

I know how simple native mysqli CRUD, still I decide to make it even more simpler cause I'm to lazy to write long code.

I'm not a pro! This library only simplified the standard database access of procedural mysqli for php code. It's nothing compare to any other popular framework.

If you aware of certain issue like security, please consider using other library.
OR...
you can modified the function in DatabaseSSQLi/function.php 
Don't worry. I don't care about license since it's too simple to be commercial use. I just feel like not keeping this thing to myself.

Fell free to fork, modified, making a pull request, etc.

By Bent_Hartz
My email: Djaka.tingkir2@gmail.com (though I rarely open the inbox)