<?php
// include this file to load the library
include "DatabaseSSQLi/function.php";

//run a query (For INSERT, UPDATE, etc)
query("INSERT INTO user VALUES(111,'Bent','be123','',1,'Admin',0)");

//Store to variable (as procedural mysqli query)
$data = query("SELECT * FROM user");

//and use it like this
foreach ($data as $row) {
	echo $row['nama']."<br>";
}

//count number of rows from stored query
$count = numrows($data);
echo "Number of row in data: " . $count . "<br>";

//count number of rows from direct query
$count = query_numrows("SELECT * FROM user");
echo "Number of row from query: " . $count . "<br>";


//make a two dimensional associative array
$data = query_assoc("SELECT * FROM user");
//it became array, use directly like this
echo $data[0]['nama']."<br>";


?>